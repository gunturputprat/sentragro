import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

// export const updateConversations = functions.firestore
//   .document("conversations/{conversationId}")
//   .onUpdate(async (convers, context) => {
//     const users = convers.after.data();
//     console.log("conversation function", users);
//     const sender = users[0];
//     const receiver = users[1];
//     const conversationId = context.params.conversationId;
//     admin
//       .firestore()
//       .collection("conversations/")
//       .doc(conversationId + "/messages")
//       .valueChanges();
//   });
export const getConversations = functions.firestore
  .document("conversations/{conversationId}")
  .onUpdate(async (event, context) => {
    const data = event.after.data().users;
    // console.log("data function", data);
    // console.log("context function", context);
    const senderId = data[0];
    const receiverId = data[1];
    const conversationId = context.params.conversationId;
    // console.log("user senderId", senderId);
    // console.log("user receiverId", receiverId);
    // console.log("all conversationId", conversationId);

    const db = admin.firestore();
    // get user name sender
    const senderName = db
      .collection("accounts/")
      .doc(senderId)
      .get()
      .then((res) => {
        const name = res.get("name");
        return name;
      });
    // get token receiver
    const token = db
      .collection("accounts/")
      .doc(receiverId)
      .get()
      .then((userReceiver) => {
        const pushToken = userReceiver.get("pushToken");
        return pushToken;
      });
    // CHECKPOINT

    const message = db
      .collection("conversations/")
      .doc(conversationId)
      .get()
      .then((messages) => {
        // get last message
        const length = messages.get("messages").length;
        const message = messages.get("messages")[length - 1].message;
        return message;
      });
    // Notification content
    const payload = {
      notification: {
        title: await senderName,
        body: await message,
        icon: "notification_icon",
      },
    };
    console.log("token", token);
    console.log("payload", payload);

    // // ref to the device collection for the user
    // const devicesRef = db.collection("devices").where("userId", "==", userId);

    // // get the user's tokens and send notifications
    // const devices = await devicesRef.get();

    // const tokens = [];

    // // send a notification to each device token
    // devices.forEach((result) => {
    //   const token = result.data().token;

    //   tokens.push(token);
    // });

    return admin.messaging().sendToDevice(await token, payload);
  });
admin.initializeApp();
