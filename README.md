# HiApp

## Prerequisites

1. We need at least Node v12
   ```
   https://nodejs.org/en/
   ```
2. We are using vscode

   ```
   https://code.visualstudio.com/
   ```

   setting.json should be uniform

3. Java SDK v8 (register a new account with your E-Mail)

   ```
   https://www.oracle.com/id/java/technologies/javase/javase-jdk8-downloads.html
   ```

4. Android Studio with all licenses accepted (follow the instruction there)
   on Linux, Mac

   ```
   ./sdkmanager --licenses
   ```

   on Windows

   ```
   cd /d "%ANDROID_SDK_ROOT%/tools/bin" sdkmanager --licenses
   ```

5) Check your Android Path, make sure you can run adb from anywhere
   on Linux, Mac

   ```
   nano ~/.bash_profile
   ```

   add this

   ```
   export ANDROID_HOME=/Users/xxx/Library/Android/sdk
   export PATH=$PATH:$ANDROID_HOME/platform-tools
   export PATH=$PATH:$ANDROID_HOME/tools
   export PATH=$PATH:$ANDROID_HOME/tools/bin
   export PATH=$PATH:$ANDROID_HOME/emulator
   export PATH="$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$ANDROID_HOME/platform-tools:$PATH"
   ```

   on Windows

   ```
   a. In Search, search for and then select: System (Control Panel)
   b. Click the Advanced system settings link.
   c. Click Environment Variables. In the section System Variables, find the PATH environment variable and select it. Click Edit. If the PATH environment variable does not exist, click New.
   d. In the Edit System Variable (or New System Variable) window, specify the value of the PATH environment variable. Click OK. Close all remaining windows by clicking OK.
   e. Reopen Command prompt window, and run your java code.
   ```

6) Install git into your machine if you don't have one

   ```
   https://git-scm.com/downloads/guis
   ```

   add your credentials,

   ```
   git config --global user.name "John Doe"
   git config --global user.email johndoe@example.com
   ```

7) Clone this Repo

   ```
   git clone https://gitlab.com/gpratama/hiapp-mobile.git
   ```

8) Install all dependencies and devDependencies

   ```
   npm install or npm i
   ```

9) ionic should be installed too

   ```
   npm install -g @ionic/cli
   ```

10) Add platforms to ionic

    ```
    ionic cordova platform add android
    ionic cordova platform add ios
    ```

11) run on browser

    ```
    ionic serve

    ```

12) run on android/ios
    ```
    ionic cordova run android -l
    ionic cordova run ios -l
    ```

## Hint

1. If you get error like this "D8: Cannot fit requested classes in a single dex file (# methods: 128983 > 65536)"
   ```
   just put this line "multiDexEnabled true" in defaultConfig
   (solved in https://stackoverflow.com/questions/55591958/flutter-firestore-causing-d8-cannot-fit-requested-classes-in-a-single-dex-file)
   ```
