import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./tabs.page";

const routes: Routes = [
  {
    path: "tabs",
    component: TabsPage,
    children: [
      {
        path: "tab1",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../stock/stock.module").then((m) => m.StockPageModule),
          },
        ],
      },
      {
        path: "tab2",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../farmers/farmers.module").then(
                (m) => m.FarmersPageModule
              ),
          },
        ],
      },
      {
        path: "tab3",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../cashflow/cashflow.module").then(
                (m) => m.CashflowPageModule
              ),
          },
        ],
      },
      {
        path: "tab4",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../messages/messages.module").then(
                (m) => m.MessagesPageModule
              ),
          },
        ],
      },
      {
        path: "tab5",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../groups/groups.module").then((m) => m.GroupsPageModule),
          },
        ],
      },
      // {
      //   path: "tab6",
      //   children: [
      //     {
      //       path: "",
      //       loadChildren: () =>
      //         import("../friends/friends.module").then(
      //           (m) => m.FriendsPageModule
      //         ),
      //     },
      //   ],
      // },
      {
        path: "tab7",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../settings/settings.module").then(
                (m) => m.SettingsPageModule
              ),
          },
        ],
      },
      {
        path: "",
        redirectTo: "/tabs/tab1",
        pathMatch: "full",
      },
    ],
  },
  {
    path: "",
    redirectTo: "/tabs/tab1",
    pathMatch: "full",
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
