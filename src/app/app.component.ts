import { Component } from "@angular/core";

import { Platform, NavController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { FCM } from "@ionic-native/fcm/ngx";
import { Router } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private afAuth: AngularFireAuth,
    private fcm: FCM,
    private navCtrl: NavController
  ) {
    this.initializeApp();
  }
  callNotification() {
    this.fcm.onNotification().subscribe((data) => {
      if (data.wasTapped) {
        //Notification was received on device tray and tapped by the user.
        console.log(JSON.stringify(data));
      } else {
        //Notification was received in foreground. Maybe the user needs to be notified.
        console.log(JSON.stringify(data));
      }
    });
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.navigationCenter();
      if (this.platform.is("android")) this.statusBar.styleLightContent();
      else this.statusBar.styleDefault();

      this.splashScreen.hide();
      if (localStorage.getItem("isLoggedIn") == "true") {
        this.callNotification();

        this.afAuth.auth.onAuthStateChanged((user) => {
          if (user == null)
            // this.router.navigateByUrl("/on-boarding", {
            //   replaceUrl: true,
            //   skipLocationChange: true,
            // });
            this.router.navigateByUrl("/login");
          else
            this.router.navigateByUrl("/tabs/tab1", {
              skipLocationChange: true,
              replaceUrl: true,
            });
        });
      } else {
        this.router.navigateByUrl("/login");
        // this.router.navigateByUrl("/on-boarding");
      }
    });
  }

  navigationCenter() {
    this.platform.backButton.subscribeWithPriority(0, () => {
      switch (this.router.url) {
        case "/tabs/tab1":
        case "/tabs/tab2":
        case "/tabs/tab3":
        case "/tabs/tab4":
        case "/tabs/tab5":
          navigator["app"].exitApp();
          break;
        default:
          this.navCtrl.pop();
          break;
      }
    });
  }
}
