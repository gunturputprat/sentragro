import { NgModule, Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { DataService } from "../services/data.service";
import { LoadingService } from "../services/loading.service";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireAuth } from "@angular/fire/auth";
import { AlertController } from "@ionic/angular";
import { ImageService } from "../services/image.service";
import { Camera } from "@ionic-native/camera/ngx";

import { Validator } from "src/environments/validator";
import { LoginService } from "../services/login.service";
import * as moment from "moment";
import { Router } from "@angular/router";

@Component({
  selector: "app-farmers-add",
  templateUrl: "./farmers-add.page.html",
  styleUrls: ["./farmers-add.page.scss"],
})
export class FarmersAddPage implements OnInit {
  farmer: any = {};
  isFarmerUploaded = false;
  myForm: FormGroup;
  submitAttempt = false;
  errorMessages: any = [];
  defaultImage = "assets/images/default-img.png";
  time: string;
  currentUser: any;

  constructor(
    private loginService: LoginService,
    private dataProvider: DataService,
    private loadingProvider: LoadingService,
    private afdb: AngularFirestore,
    private afAuth: AngularFireAuth,
    private alertCtrl: AlertController,
    private imageProvider: ImageService,
    private camera: Camera,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.errorMessages = Validator.errorMessages;
    this.myForm = this.formBuilder.group({
      farmer_name: Validator.farmer_nameValidator,
      phoneNumber: Validator.phoneNumberValidator,
      plantation: Validator.plantationValidator,
      address: Validator.addressValidator,
    });
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
  }

  save() {
    this.time = moment(new Date()).format("YYYY-MM-DD-HH-mm-ss");
    console.log("date", this.time);
    this.submitAttempt = true;
    console.log("farmer new", this.farmer);
    this.farmer.userId = this.currentUser.uid;
    this.farmer.dateCreated = new Date();
    this.farmer.farmerId = this.time;
    this.farmer.img = "./assets/images/default-dp.png";
    this.farmer.ktp = "./assets/images/default-ktp.png";
    if (this.myForm.valid) {
      this.loadingProvider.show();
      // add new farmer
      this.dataProvider
        .setFarmers(this.time)
        .set(this.farmer)
        .then((response) => {
          console.log("response upload farmer", response);
          this.loadingProvider.showToast("Berhasil menambahkan produk");
          this.isFarmerUploaded = true;
          this.loadingProvider.hide();
        })
        .catch((err) => {
          this.loadingProvider.showToast(
            "Terjadi kesalahan, silahkan coba lagi"
          );
          this.loadingProvider.hide();
        });
      // insert farmer id as subcollection account
      this.dataProvider
        .getUser(this.currentUser.uid)
        .collection("farmers")
        .doc(this.time)
        .set({ farmerId: this.time });
    }
  }

  setPhoto() {
    this.alertCtrl
      .create({
        header: "Set Profile Photo",
        message:
          "Do you want to take a photo or choose from your photo gallery?",
        buttons: [
          {
            text: "Cancel",
            handler: (data) => {},
          },
          {
            text: "Choose from Gallery",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setFarmerPhoto(
                this.time,
                this.camera.PictureSourceType.PHOTOLIBRARY
              );
            },
          },
          {
            text: "Take Photo",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setFarmerPhoto(
                this.time,
                this.camera.PictureSourceType.CAMERA
              );
            },
          },
        ],
      })
      .then((r) => r.present());
  }

  setPhotoKTP() {
    this.alertCtrl
      .create({
        header: "Set KTP Photo",
        message:
          "Do you want to take a photo or choose from your photo gallery?",
        buttons: [
          {
            text: "Cancel",
            handler: (data) => {},
          },
          {
            text: "Choose from Gallery",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setKTPFarmerPhoto(
                this.farmer.farmerId,
                this.camera.PictureSourceType.PHOTOLIBRARY
              );
            },
          },
          {
            text: "Take Photo",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setKTPFarmerPhoto(
                this.farmer.farmerId,
                this.camera.PictureSourceType.CAMERA
              );
            },
          },
        ],
      })
      .then((r) => r.present());
  }
  imageUploaded() {
    this.router.navigateByUrl("/tabs/tab2");
  }
}
