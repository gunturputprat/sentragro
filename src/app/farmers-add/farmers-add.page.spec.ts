import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmersAddPage } from './farmers-add.page';

describe('FarmersAddPage', () => {
  let component: FarmersAddPage;
  let fixture: ComponentFixture<FarmersAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmersAddPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmersAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
