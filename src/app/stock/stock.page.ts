import { Component, OnInit } from "@angular/core";
import { DataService } from "../services/data.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-stock",
  templateUrl: "./stock.page.html",
  styleUrls: ["./stock.page.scss"],
})
export class StockPage implements OnInit {
  currentUser = JSON.parse(localStorage.getItem("currentUser"));
  listProducts = [];
  constructor(private dataProvider: DataService, private router: Router) {}

  ngOnInit() {}
  ionViewDidEnter() {
    this.getProducts();
  }
  ionViewDidLeave() {
    this.listProducts = [];
  }

  getProducts() {
    this.dataProvider
      .getProducts(this.currentUser.uid)
      .valueChanges()
      .subscribe((products) => {
        let tempProduct = [];
        if (products) {
          tempProduct = products;
          tempProduct.forEach((product) => {
            this.dataProvider
              .getProduct(product.productId)
              .valueChanges()
              .subscribe((res) => {
                this.listProducts.push(res);
              });
          });
        }
      });
  }

  detail(productId) {
    console.log("detail", productId);
    this.router.navigateByUrl("/stock-detail/" + productId);
  }
}
