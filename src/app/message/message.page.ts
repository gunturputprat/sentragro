import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import * as firebase from "firebase";
import { DataService } from "../services/data.service";
import { Camera } from "@ionic-native/camera/ngx";
import {
  ActionSheetController,
  AlertController,
  ModalController,
  IonContent,
} from "@ionic/angular";
import { AngularFirestore } from "@angular/fire/firestore";
import { LoadingService } from "../services/loading.service";
import { ImageService } from "../services/image.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Contacts } from "@ionic-native/contacts/ngx";
import { Keyboard } from "@ionic-native/keyboard/ngx";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { ImagemodalPage } from "../imagemodal/imagemodal.page";
import { FiletransferService } from "../services/filetransfer.service";

@Component({
  selector: "app-message",
  templateUrl: "./message.page.html",
  styleUrls: ["./message.page.scss"],
})
export class MessagePage implements OnInit {
  @ViewChild(IonContent, null) contentArea: IonContent;

  userId: any;
  title: any;
  message: any;
  conversationId: any;
  messages: any;
  updateDateTime: any;
  messagesToShow: any;
  startIndex: any = -1;
  // Set number of messages to show.
  numberOfMessages = 10;
  loggedInUserId: any;
  token: any;
  document: unknown;
  filename: any;

  // MessagePage
  // This is the page where the user can chat with a friend.
  constructor(
    // public navCtrl: NavController,
    // public navParams: NavParams,
    private router: Router,
    private route: ActivatedRoute,
    private dataProvider: DataService,
    private angularfire: AngularFirestore,
    private loadingProvider: LoadingService,
    private alertCtrl: AlertController,
    private imageProvider: ImageService,
    private modalCtrl: ModalController,
    private camera: Camera,
    private keyboard: Keyboard,
    private actionSheet: ActionSheetController,
    private contacts: Contacts,
    private geolocation: Geolocation,
    private documentTransfer: FiletransferService
  ) {}

  ngOnInit() {
    // this.scrollBottom();
  }

  ionViewDidEnter() {
    this.userId = this.route.snapshot.params.id;
    this.loggedInUserId = firebase.auth().currentUser.uid;
    console.log("friend:", this.userId + " me: ", this.loggedInUserId);

    // Get friend details.
    this.dataProvider
      .getUser(this.userId)
      .valueChanges()
      .subscribe((user: any) => {
        this.title = user.name;
        this.token = user.pushToken;
      });

    // Get conversationInfo with friend.
    this.angularfire
      .collection("/accounts/")
      .doc(this.loggedInUserId + "/conversations/" + this.userId)
      .valueChanges()
      .subscribe((conversation: any) => {
        console.log("conversation personal", conversation);
        if (conversation) {
          // User already have conversation with this friend, get conversation
          this.conversationId = conversation.conversationId;
          console.log("this.conversationId:", this.conversationId);
          this.scrollBottom();

          // Get conversation
          this.dataProvider
            .getConversationMessages(this.conversationId)
            .valueChanges()
            .subscribe((messagesRes: any) => {
              let messages = messagesRes.messages;
              console.log("messages:", messages);
              if (messages == null) messages = [];
              if (this.messages) {
                // Just append newly added messages to the bottom of the view.
                if (messages.length > this.messages.length) {
                  let message = messages[messages.length - 1];

                  this.dataProvider
                    .getUser(message.sender)
                    .valueChanges()
                    .subscribe((user: any) => {
                      message.avatar = user.img;
                    });
                  this.messages.push(message);
                  this.messagesToShow.push(message);
                }
              } else {
                // Get all messages, this will be used as reference object for messagesToShow.
                this.messages = [];
                messages.forEach((message) => {
                  this.dataProvider
                    .getUser(message.sender)
                    .valueChanges()
                    .subscribe((user: any) => {
                      message.avatar = user.img;
                    });
                  this.messages.push(message);
                });
                // Load messages in relation to numOfMessages.
                if (this.startIndex == -1) {
                  // Get initial index for numberOfMessages to show.
                  if (this.messages.length - this.numberOfMessages > 0) {
                    this.startIndex =
                      this.messages.length - this.numberOfMessages;
                  } else {
                    this.startIndex = 0;
                  }
                }
                if (!this.messagesToShow) {
                  this.messagesToShow = [];
                }
                // Set messagesToShow
                for (var i = this.startIndex; i < this.messages.length; i++) {
                  this.messagesToShow.push(this.messages[i]);
                }
                this.loadingProvider.hide();
              }
            });
        }
      });

    // Update messages' date time elapsed every minute based on Moment.js.
    var that = this;
    if (!that.updateDateTime) {
      that.updateDateTime = setInterval(function () {
        if (that.messages) {
          that.messages.forEach((message) => {
            let date = message.date;
            message.date = new Date(date);
          });
        }
      }, 60000);
    }
  }
  // Load previous messages in relation to numberOfMessages.
  loadPreviousMessages() {
    var that = this;
    // Show loading.
    this.loadingProvider.show();
    setTimeout(function () {
      // Set startIndex to load more messages.
      if (that.startIndex - that.numberOfMessages > -1) {
        that.startIndex -= that.numberOfMessages;
      } else {
        that.startIndex = 0;
      }
      // Refresh our messages list.
      that.messages = null;
      that.messagesToShow = null;

      that.scrollTop();

      // Populate list again.
      that.ionViewDidEnter();
    }, 1000);
  }

  // Update messagesRead when user lefts this page.
  ionViewWillLeave() {
    this.setMessagesRead();
  }

  // Check if currentPage is active, then update user's messagesRead.
  setMessagesRead() {
    console.log("setMessagesRead conversationid", this.conversationId);
    this.dataProvider
      .getConversationMessages(this.conversationId)
      .valueChanges()
      .subscribe((snap: any) => {
        console.log("snap message new", snap.messages.length);
        if (snap != null) {
          this.angularfire
            .collection("/accounts/")
            .doc(this.loggedInUserId + "/conversations/" + this.userId)
            .update({
              messagesRead: snap.messages.length,
            });
        }
      });
  }

  scrollBottom() {
    console.log("Calling Sb");
    setTimeout(() => {
      if (this.contentArea.scrollToBottom) {
        this.contentArea.scrollToBottom();
      }
    }, 500);
    this.setMessagesRead();
  }

  scrollTop() {
    console.log("Calling St");
    setTimeout(() => {
      if (this.contentArea.scrollToTop) {
        this.contentArea.scrollToTop();
      }
    }, 500);
  }

  // Check if the user is the sender of the message.
  isSender(message) {
    if (message.sender == this.loggedInUserId) {
      return true;
    } else {
      return false;
    }
  }

  // Send message, if there's no conversation yet, create a new conversation.
  send(type) {
    if (this.message) {
      // User entered a text on messagebox
      if (this.conversationId) {
        let messages = JSON.parse(JSON.stringify(this.messages));
        messages.push({
          date: new Date().toString(),
          sender: this.loggedInUserId,
          filename: this.filename,
          type: type,
          message: this.message,
        });

        // Update conversation on database.
        var users = [];
        users.push(this.loggedInUserId);
        users.push(this.userId);
        this.dataProvider.getConversation(this.conversationId).update({
          messages: messages,
          users: users,
        });
        // Clear messagebox.
        this.message = "";
        this.scrollBottom();
      } else {
        console.log("else");
        // New Conversation with friend.
        var messages = [];
        messages.push({
          date: new Date().toString(),
          sender: this.loggedInUserId,
          filename: this.filename,
          type: type,
          message: this.message,
        });
        var users = [];
        users.push(this.loggedInUserId);
        users.push(this.userId);
        // Add conversation.
        this.angularfire
          .collection("conversations")
          .add({
            dateCreated: new Date().toString(),
            messages: messages,
            users: users,
          })
          .then((success) => {
            console.log("success create new chat", success);

            let conversationId = success.id;
            this.message = "";
            // Add conversation reference to the users.
            this.angularfire
              .collection("/accounts/")
              .doc(this.loggedInUserId + "/conversations/" + this.userId)
              .set({
                key: this.userId,
                conversationId: conversationId,
                messagesRead: 1,
              });
            this.angularfire
              .collection("/accounts/")
              .doc(this.userId + "/conversations/" + this.loggedInUserId)
              .set({
                key: this.loggedInUserId,
                conversationId: conversationId,
                messagesRead: 0,
              });
          });
        this.scrollBottom();
      }
    }
  }

  viewUser(userId) {
    this.router.navigateByUrl("userinfo/" + userId);
  }

  attach() {
    this.actionSheet
      .create({
        header: "Choose attachments",

        buttons: [
          {
            text: "Document",
            handler: () => {
              console.log(
                "clicked document. conversationId",
                this.conversationId
              );
              this.documentTransfer
                .chooseFile(this.conversationId)
                .then(([filename, url, filetype]) => {
                  console.log("return file document", filename, url, filetype);
                  this.message = url;
                  this.filename = filename;
                  this.send(filetype);
                });
            },
          },
          {
            text: "Camera",
            handler: () => {
              this.imageProvider
                .uploadPhotoMessage(
                  this.conversationId,
                  this.camera.PictureSourceType.CAMERA
                )
                .then((url) => {
                  this.message = url;
                  this.send("image");
                });
            },
          },
          {
            text: "Photo Library",
            handler: () => {
              this.imageProvider
                .uploadPhotoMessage(
                  this.conversationId,
                  this.camera.PictureSourceType.PHOTOLIBRARY
                )
                .then((url) => {
                  this.message = url;
                  this.send("image");
                });
            },
          },
          {
            text: "Video",
            handler: () => {
              this.imageProvider
                .uploadVideoMessage(this.conversationId)
                .then((url) => {
                  this.message = url;
                  this.send("video");
                });
            },
          },
          // {
          //   text: "Location",
          //   handler: () => {
          //     this.geolocation
          //       .getCurrentPosition({
          //         timeout: 5000,
          //       })
          //       .then(
          //         (res) => {
          //           let locationMessage =
          //             "Location:<br> lat:" +
          //             res.coords.latitude +
          //             "<br> lng:" +
          //             res.coords.longitude;
          //           let mapUrl =
          //             "<a href='https://www.google.com/maps/search/" +
          //             res.coords.latitude +
          //             "," +
          //             res.coords.longitude +
          //             "'>View on Map</a>";

          //           let confirm = this.alertCtrl
          //             .create({
          //               header: "Your Location",
          //               message: locationMessage,
          //               buttons: [
          //                 {
          //                   text: "cancel",
          //                   handler: () => {
          //                     console.log("canceled");
          //                   },
          //                 },
          //                 {
          //                   text: "Share",
          //                   handler: () => {
          //                     this.message = locationMessage + "<br>" + mapUrl;
          //                     this.send("location");
          //                   },
          //                 },
          //               ],
          //             })
          //             .then((r) => r.present());
          //         },
          //         (locationErr) => {
          //           console.log("Location Error" + JSON.stringify(locationErr));
          //         }
          //       );
          //   },
          // },
          // {
          //   text: "Contact",
          //   handler: () => {
          //     this.contacts.pickContact().then(
          //       (data) => {
          //         let name;
          //         if (data.displayName !== null) name = data.displayName;
          //         else name = data.name.givenName + " " + data.name.familyName;
          //         this.message =
          //           "<b>Name:</b> " +
          //           name +
          //           "<br><b>Mobile:</b> <a href='tel:" +
          //           data.phoneNumbers[0].value +
          //           "'>" +
          //           data.phoneNumbers[0].value +
          //           "</a>";
          //         this.send("contact");
          //       },
          //       (err) => {
          //         console.log(err);
          //       }
          //     );
          //   },
          // },
          {
            text: "cancel",
            role: "cancel",
            handler: () => {
              console.log("cancelled");
            },
          },
        ],
      })
      .then((r) => r.present());
  }

  // Enlarge image messages.
  enlargeImage(img) {
    this.modalCtrl
      .create({
        component: ImagemodalPage,
        componentProps: {
          img: img,
        },
      })
      .then((res) => {
        res.present();
      });
    // let imageModal = this.modalCtrl.create(ImageModalPage, { img: img });
    // imageModal.present();
  }
}
