import { Component, OnInit } from "@angular/core";
import {
  Contacts,
  Contact,
  ContactField,
  ContactName,
} from "@ionic-native/contacts/ngx";
import { DataService } from "../services/data.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-contact",
  templateUrl: "./contact.page.html",
  styleUrls: ["./contact.page.scss"],
})
export class ContactPage implements OnInit {
  allContacts = [];
  friendContacts = [];

  constructor(
    private router: Router,
    private dataServices: DataService,
    private contacts: Contacts
  ) {}

  ngOnInit() {
    this.contactListAdded();
    this.getContactList();
  }

  contactListAdded() {
    this.dataServices
      .getUserByPhone()
      .valueChanges()
      .subscribe((res) => {
        this.allContacts = res;
      });
  }

  getContactList() {
    let index = 0;
    this.contacts
      .find(["displayName", "name", "phoneNumbers"], {
        multiple: true,
      })
      .then((response) => {
        for (let i = 0; i < response.length; i++) {
          for (let j = 0; j < this.allContacts.length; j++) {
            if (
              response[i].phoneNumbers[0].value ==
              this.allContacts[j].phoneNumber
            ) {
              this.friendContacts[j] = {
                name: response[i].displayName,
                phoneNumber: this.allContacts[j].phoneNumber,
                userId: this.allContacts[j].userId,
                img: this.allContacts[j].img,
              };
            }
          }
        }
      });
  }

  message(userId) {
    this.router.navigateByUrl("/message/" + userId);
  }
}
