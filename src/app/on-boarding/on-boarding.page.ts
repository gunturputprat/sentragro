import { Component, OnInit } from "@angular/core";
import { LoginService } from "../services/login.service";
import { LoadingService } from "../services/loading.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-on-boarding",
  templateUrl: "./on-boarding.page.html",
  styleUrls: ["./on-boarding.page.scss"],
})
export class OnBoardingPage implements OnInit {
  isLogoTapped: boolean = false;
  isStarted: boolean = false;
  isVerificationCodeSent: boolean = false;
  isRegistrationCompleted: boolean = false;
  isNewUser: boolean = false;
  isButtonResendActive = false;
  phoneNumber: string = "";
  verificationCode: any = null;
  verificationId: any;
  profileName: string = "";
  userUID: any;
  userEmail: any;
  maxTime = 120;
  minutes: any;
  seconds: any;

  constructor(
    private loginService: LoginService,
    private loadingProvider: LoadingService,
    private router: Router
  ) {}

  ngOnInit() {
    // countdown start
  }

  handleTapClick() {
    this.isLogoTapped = true;
  }

  handlePrivacyClick() {
    console.log("Privacy clicked");
  }

  handleToSClick() {
    console.log("Terms of Service clicked");
  }

  handleStartedClick() {
    this.isStarted = true;
  }

  handleNextClick() {
    this.loginService
      .verifyPhone("+62" + this.phoneNumber, this.maxTime)
      .then((res: any) => {
        if (res) {
          this.verificationId = res;
          this.loadingProvider.hide();
          alert("The verification has been sent to your phone");

          //console.log(this.verificationId);
          if (this.verificationId !== null) {
            this.isVerificationCodeSent = true;
            this.maxTime = 120;
            this.handleCountdownResendCode();
          }
        }
      })
      .catch((err) => {
        console.log(err);
        this.loadingProvider.hide();
        alert("The phone number is incorrect, please enter the valid number");
      });
  }

  handleEditPhoneNumber() {
    this.isVerificationCodeSent = false;
    this.isStarted = true;
  }

  handleVerificationCode(event: any) {
    if (JSON.stringify(this.verificationCode).length >= 6) {
      this.loginService
        .phoneLogin(JSON.stringify(this.verificationCode), this.verificationId)
        .then((success) => {
          /* console.log(
            "success isNewUser",
            success.additionalUserInfo.isNewUser
            ); */

          this.isRegistrationCompleted = true;
          this.userUID = success.user.uid;
          this.userEmail = success.user.email;
          this.isNewUser = success.additionalUserInfo.isNewUser;

          if (!success.additionalUserInfo.isNewUser) {
            localStorage.setItem("isLoggedIn", "true");
            this.router.navigateByUrl("/");
            this.loadingProvider.hide();
          } else {
            console.log("Success", success);
          }
        });
    }
  }

  handleCountdownResendCode() {
    const timer = setTimeout((x) => {
      // handleCountdownResendCode() was called
      let start = Date.now(),
        diff,
        minutes,
        seconds;
      diff = this.maxTime - (((Date.now() - start) / 1000) | 0);

      // does the same job as parseInt truncates the float
      minutes = (diff / 60) | 0;
      seconds = diff % 60 | 0;
      if (this.maxTime <= 0) {
        // console.log("time 1", this.maxTime);
      }
      this.maxTime -= 1;

      if (this.maxTime >= -1) {
        // console.log("time 2", minutes, seconds);
        this.isButtonResendActive = false;
        this.minutes = minutes;
        this.seconds = seconds;
        this.handleCountdownResendCode();
      } else {
        console.log("time 3", this.maxTime, minutes, seconds);
        this.isButtonResendActive = true;
      }
    }, 1000);
  }

  handleResendCode() {
    this.loginService
      .verifyPhone("+62" + this.phoneNumber, this.maxTime)
      .then((res: any) => {
        if (res) {
          this.verificationId = res;
          this.loadingProvider.hide();
          this.maxTime = 120;
          alert("The verification has been re-sent to your phone");
          this.handleCountdownResendCode();
        }
      })
      .catch((err) => {
        console.log(err);
        this.loadingProvider.hide();
        alert("The phone number is incorrect, please enter the valid number");
      });
  }

  handleProfilePicture() {
    console.log("handleProfilePicture");
  }

  handleStartMessaging() {
    this.loginService.createNewUser(
      this.userUID,
      this.profileName,
      "_",
      this.phoneNumber,
      this.userEmail,
      "I am available",
      "Phone",
      "./assets/images/default-ava.png"
    );
  }
}
