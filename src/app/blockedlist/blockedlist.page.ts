import { Component, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireAuth } from "@angular/fire/auth";
import { DataService } from "../services/data.service";

@Component({
  selector: "app-blockedlist",
  templateUrl: "./blockedlist.page.html",
  styleUrls: ["./blockedlist.page.scss"],
})
export class BlockedlistPage implements OnInit {
  blockedList: any = [];

  constructor(
    private afAuth: AngularFireAuth,
    private afdb: AngularFirestore,
    private dataProvider: DataService
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    // this.dataProvider.getBlockedLists().snapshotChanges().subscribe(conversations => {
    //   let tmp = [];
    //   console.log("conversations bloceedlist", conversations)
    //   conversations.forEach(conversation => {
    //     // fetch blocked conversation & user info
    //     this.dataProvider.getUser(conversation.key).snapshotChanges().subscribe((data: any) => {
    //       tmp.push({ key: conversation.key, name: data.payload.val().name, img: data.payload.val().img });
    //     });
    //   })
    //   console.log(tmp);
    //   this.blockedList = tmp;
    // });
  }

  unblock(uid) {
    console.log(uid);
    this.afdb
      .collection("accounts/")
      .doc(this.afAuth.auth.currentUser.uid + "/conversations/" + uid)
      .set({
        blocked: false,
      });
  }
}
