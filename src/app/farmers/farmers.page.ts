import { Component, OnInit } from "@angular/core";
import { DataService } from "../services/data.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-farmers",
  templateUrl: "./farmers.page.html",
  styleUrls: ["./farmers.page.scss"],
})
export class FarmersPage implements OnInit {
  currentUser = JSON.parse(localStorage.getItem("currentUser"));
  listFarmers = [];
  constructor(private dataProvider: DataService, private router: Router) {}

  ngOnInit() {}
  ionViewDidEnter() {
    this.getFarmers();
  }
  ionViewDidLeave() {
    this.listFarmers = [];
  }
  getFarmers() {
    this.dataProvider
      .getFarmers(this.currentUser.uid)
      .valueChanges()
      .subscribe((farmers) => {
        let tempFarmer = [];
        if (farmers) {
          tempFarmer = farmers;
          tempFarmer.forEach((farmer) => {
            this.dataProvider
              .getFarmer(farmer.farmerId)
              .valueChanges()
              .subscribe((res) => {
                this.listFarmers.push(res);
              });
          });
        }
      });
  }

  detail(farmerId) {
    console.log("detail", farmerId);
    this.router.navigateByUrl("/farmers-detail/" + farmerId);
  }
}
