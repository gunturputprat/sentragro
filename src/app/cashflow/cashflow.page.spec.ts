import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashflowPage } from './cashflow.page';

describe('CashflowPage', () => {
  let component: CashflowPage;
  let fixture: ComponentFixture<CashflowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashflowPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashflowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
