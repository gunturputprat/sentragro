import { NgModule, Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { DataService } from "../services/data.service";
import { LoadingService } from "../services/loading.service";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireAuth } from "@angular/fire/auth";
import { AlertController } from "@ionic/angular";
import { ImageService } from "../services/image.service";
import { Camera } from "@ionic-native/camera/ngx";

import { Validator } from "src/environments/validator";
import { LoginService } from "../services/login.service";
import * as moment from "moment";

@Component({
  selector: "app-stock-add",
  templateUrl: "./stock-add.page.html",
  styleUrls: ["./stock-add.page.scss"],
})
export class StockAddPage implements OnInit {
  product: any = {};
  isProductUploaded = false;
  myForm: FormGroup;
  submitAttempt = false;
  errorMessages: any = [];
  defaultImage = "assets/images/default-img.png";
  time: string;
  currentUser: any;

  constructor(
    private loginService: LoginService,
    private dataProvider: DataService,
    private loadingProvider: LoadingService,
    private afdb: AngularFirestore,
    private afAuth: AngularFireAuth,
    private alertCtrl: AlertController,
    private imageProvider: ImageService,
    private camera: Camera,
    private formBuilder: FormBuilder
  ) {
    this.errorMessages = Validator.errorMessages;
    this.myForm = this.formBuilder.group({
      product_name: Validator.product_nameValidator,
      category: Validator.categoryValidator,
      price: Validator.priceValidator,
      weight: Validator.weightValidator,
      benefits: Validator.benefitsValidator,
      ingredients: Validator.ingredientsValidator,
    });
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
  }

  save() {
    this.time = moment(new Date()).format("YYYY-MM-DD-HH-mm-ss");
    console.log("date", this.time);
    this.submitAttempt = true;
    console.log("product new", this.product);
    this.product.userId = this.currentUser.uid;
    this.product.dateCreated = new Date();
    this.product.productId = this.time;
    this.product.img = "./assets/images/default-product.png";
    if (this.myForm.valid) {
      this.loadingProvider.show();
      // add new product
      this.dataProvider
        .setProducts(this.time)
        .set(this.product)
        .then((response) => {
          console.log("response upload product", response);
          this.loadingProvider.showToast("Berhasil menambahkan produk");
          this.isProductUploaded = true;
          this.loadingProvider.hide();
        })
        .catch((err) => {
          this.loadingProvider.showToast(
            "Terjadi kesalahan, silahkan coba lagi"
          );
          this.loadingProvider.hide();
        });
      // insert product id as subcollection account
      this.dataProvider
        .getUser(this.currentUser.uid)
        .collection("products")
        .doc(this.time)
        .set({ productId: this.time });
    }
  }

  setPhoto() {
    this.alertCtrl
      .create({
        header: "Set Profile Photo",
        message:
          "Do you want to take a photo or choose from your photo gallery?",
        buttons: [
          {
            text: "Cancel",
            handler: (data) => {},
          },
          {
            text: "Choose from Gallery",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setProductPhoto(
                this.time,
                this.camera.PictureSourceType.PHOTOLIBRARY
              );
            },
          },
          {
            text: "Take Photo",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setProductPhoto(
                this.time,
                this.camera.PictureSourceType.CAMERA
              );
            },
          },
        ],
      })
      .then((r) => r.present());
  }
}
