import { Injectable } from "@angular/core";
import * as firebase from "firebase";

declare let window: any;

@Injectable({
  providedIn: "root",
})
export class EventdataService {
  docStorage: any;

  constructor() {
    this.docStorage = firebase.storage().ref("/documents");
  }
  makeFileIntoBlob(_imagePath, name, type) {
    // INSTALL PLUGIN - cordova plugin add cordova-plugin-file
    console.log("_imagePath", _imagePath);
    return new Promise((resolve, reject) => {
      window.resolveLocalFileSystemURL(_imagePath, (uriEntry: any) => {
        console.log("uriEntry", uriEntry);
        uriEntry.file((resFile) => {
          var reader = new FileReader();
          reader.onloadend = (evt: any) => {
            var imgBlob: any = new Blob([evt.target.result], { type: type });
            imgBlob.name = name;
            resolve(imgBlob);
          };

          reader.onerror = (e) => {
            alert("Failed file read: " + e.toString());
            reject(e);
          };

          reader.readAsArrayBuffer(resFile);
        });
      });
    });
  }

  getfilename(filestring) {
    let file;
    file = filestring.replace(/^.*[\\\/]/, "");
    return file;
  }

  getfileext(filestring) {
    let file = filestring.substr(filestring.lastIndexOf(".") + 1);
    return file;
  }
  getRequestFiles(conversationId): any {
    return this.docStorage.child(conversationId);
  }

  addAssignmentFile(conversationId, file: any): any {
    return new Promise((resolve) => {
      this.docStorage
        .child(conversationId + "/" + file.filename)
        //Saves the file to storage
        .put(file.blob, { contentType: file.type })
        .then((savedFile) => {
          console.log("file", file);
          //Gets the file url and saves it in the database
          this.docStorage
            .child(conversationId + "/" + file.filename)
            .getDownloadURL()
            .then((url) => {
              file.downloadUrl = url;
              resolve([file.filename, url, file.fileext]);
            });
        });
    });
  }
}
