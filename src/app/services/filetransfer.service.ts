import { Injectable } from "@angular/core";
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject,
} from "@ionic-native/file-transfer/ngx";
import { File } from "@ionic-native/file/ngx";
import { FileChooser } from "@ionic-native/file-chooser/ngx";
import { FilePath } from "@ionic-native/file-path/ngx";
import { EventdataService } from "./eventdata.service";
declare var cordova: any;

@Injectable({
  providedIn: "root",
})
export class FiletransferService {
  private fileTransfer: FileTransferObject = this.transfer.create();
  sbaid: any;
  constructor(
    private fileChooser: FileChooser,
    private file: File,
    private transfer: FileTransfer,
    private filePath: FilePath,
    private eventsdata: EventdataService
  ) {}

  chooseFile(conversationId) {
    return new Promise((resolve) => {
      let file;
      this.fileChooser.open().then((uri) => {
        this.filePath.resolveNativePath(uri).then((fileentry) => {
          let filename = this.eventsdata.getfilename(fileentry);
          let fileext = this.eventsdata.getfileext(fileentry);
          console.log("fileext", fileext);
          switch (fileext) {
            case "pdf":
              this.eventsdata
                .makeFileIntoBlob(uri, fileext, "application/pdf")
                .then((fileblob) => {
                  file = {
                    blob: fileblob,
                    type: "application/pdf",
                    fileext: fileext,
                    filename: filename,
                  };
                  resolve(
                    this.eventsdata.addAssignmentFile(conversationId, file)
                  );
                });
              break;

            case "docx":
              this.eventsdata
                .makeFileIntoBlob(
                  uri,
                  fileext,
                  "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                )
                .then((fileblob) => {
                  file = {
                    blob: fileblob,
                    type:
                      "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    fileext: fileext,
                    filename: filename,
                  };
                  resolve(
                    this.eventsdata.addAssignmentFile(conversationId, file)
                  );
                });
              break;

            case "doc":
              this.eventsdata
                .makeFileIntoBlob(uri, fileext, "application/msword")
                .then((fileblob) => {
                  file = {
                    blob: fileblob,
                    type: "application/msword",
                    fileext: fileext,
                    filename: filename,
                  };
                  resolve(
                    this.eventsdata.addAssignmentFile(conversationId, file)
                  );
                });
              break;
            case "epub":
              this.eventsdata
                .makeFileIntoBlob(uri, fileext, "application/octet-stream")
                .then((fileblob) => {
                  file = {
                    blob: fileblob,
                    type: "application/octet-stream",
                    fileext: fileext,
                    filename: filename,
                  };
                  resolve(
                    this.eventsdata.addAssignmentFile(conversationId, file)
                  );
                });
              break;
            case "accdb":
              this.eventsdata
                .makeFileIntoBlob(uri, filename, "application/msaccess")
                .then((fileblob) => {
                  file = {
                    blob: fileblob,
                    type: "application/msaccess",
                    fileext: fileext,
                    filename: filename,
                  };
                  resolve(
                    this.eventsdata.addAssignmentFile(conversationId, file)
                  );
                });
              break;
            case "xlsx":
              this.eventsdata
                .makeFileIntoBlob(
                  uri,
                  filename,
                  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                )
                .then((fileblob) => {
                  file = {
                    blob: fileblob,
                    type:
                      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileext: fileext,
                    filename: filename,
                  };
                  resolve(
                    this.eventsdata.addAssignmentFile(conversationId, file)
                  );
                });
              break;

            default:
              alert("The format ." + fileext + " can't be uploaded.");
              break;
          }
        });
      });
    });
  }
  gotoFilePage(file) {
    cordova.InAppBrowser.open(file, "_system", "location=yes");
  }
}
