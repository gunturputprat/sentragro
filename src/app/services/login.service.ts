import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { auth } from "firebase/app";
import { AngularFirestore } from "@angular/fire/firestore";
import * as firebase from "firebase";
import { LoadingService } from "./loading.service";
import { Platform } from "@ionic/angular";

import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { Facebook } from "@ionic-native/facebook/ngx";

import { environment } from "src/environments/environment.prod";
import { Router } from "@angular/router";
import { FirebaseAuthentication } from "@ionic-native/firebase-authentication/ngx";

@Injectable({
  providedIn: "root",
})
export class LoginService {
  user: any = {};
  verificationId: any;
  phoneNumber: any;
  chapta: any;

  constructor(
    private afAuth: AngularFireAuth,
    private afdb: AngularFirestore,
    private loadingProvider: LoadingService,
    private platform: Platform,
    private gplus: GooglePlus,
    private facebook: Facebook,
    private router: Router,
    private firebaseAuthentication: FirebaseAuthentication
  ) {}

  login(email, password) {
    this.loadingProvider.show();
    this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        console.log(res);
        localStorage.setItem("isLoggedIn", "true");
        localStorage.setItem("loggedInUserId", res.user.uid);
        localStorage.setItem("currentUser", JSON.stringify(res.user));
        this.router.navigateByUrl("/");
        this.loadingProvider.hide();
      })
      .catch((err) => {
        console.log(err);
        this.loadingProvider.hide();
        this.loadingProvider.showToast(err.message);
      });
  }

  register(name, username, email, password, img) {
    this.loadingProvider.show();
    this.afAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then((res) => {
        let user: any = this.afAuth.auth.currentUser;
        localStorage.setItem("isLoggedIn", "true");
        localStorage.setItem("currentUser", JSON.stringify(this.afAuth.auth));
        this.loadingProvider.hide();
        this.createNewUser(
          user.uid,
          name,
          username,
          "0888888888",
          user.email,
          "I am available",
          "Firebase",
          img
        );
      })
      .catch((err) => {
        console.log(err);
        this.loadingProvider.hide();
        this.loadingProvider.showToast(err.message);
      });
  }

  reset(email) {
    console.log(email);
    this.loadingProvider.show();
    this.afAuth.auth
      .sendPasswordResetEmail(email)
      .then(() => {
        this.loadingProvider.hide();
        this.loadingProvider.showToast("Please Check your inbox");
      })
      .catch((err) => {
        this.loadingProvider.hide();
        this.loadingProvider.showToast(err.message);
      });
  }
  /* verifyPhone(phoneNumber) {
    this.loadingProvider.show();
    this.firebaseAuthentication
      .verifyPhoneNumber(phoneNumber, 60)
      .then((res: any) => {
        this.phoneNumber = phoneNumber;
        if (res) {
          this.verificationId = res;
          console.log("res verifyphone,", res);
          this.loadingProvider.hide();
          alert("The code has been sent to your phone");
        }
      })
      .catch((err) => {
        console.log(err);
        this.loadingProvider.hide();
        alert("The phone number is incorrect, please enter the valid number");
      });
  } */

  verifyPhone(phoneNumber: string, countdownResendCode) {
    this.loadingProvider.show();
    return this.firebaseAuthentication.verifyPhoneNumber(
      phoneNumber,
      countdownResendCode
    );
  }

  /* phoneLogin(numberCode) {
    this.loadingProvider.show();
    console.log("verificationId", this.verificationId);
    console.log("numberCode", numberCode);
    let signInCredential = firebase.auth.PhoneAuthProvider.credential(
      this.verificationId,
      numberCode
    );
    firebase
      .auth()
      .signInWithCredential(signInCredential)
      .then((success) => {
        console.log("success isNewUser", success.additionalUserInfo.isNewUser);
        console.log("success user", success.user);
        if (success.additionalUserInfo.isNewUser == true) {
          this.createNewUser(
            success.user.uid,
            "name",
            "username",
            this.phoneNumber,
            success.user.email,
            "I am available",
            "Phone",
            "./assets/images/default-dp.png"
          );
        } else {
          localStorage.setItem("isLoggedIn", "true");
          localStorage.setItem("loggedInUserId", success.user.uid);

          console.log("user email", success.user);
          this.router.navigateByUrl("/");
        }
      })
      .catch((err) => {
        console.log(err);
        this.loadingProvider.hide();
        alert("There is something happen, please check the code");
      });
  } */

  phoneLogin(numberCode: any, verificationId: any) {
    console.log("code:", numberCode, " verifId", verificationId);
    let signInCredential = firebase.auth.PhoneAuthProvider.credential(
      verificationId,
      numberCode
    );
    return firebase.auth().signInWithCredential(signInCredential);
  }

  fbLogin() {
    if (this.platform.is("desktop")) {
      this.loadingProvider.show();
      this.afAuth.auth
        .signInWithPopup(new auth.FacebookAuthProvider())
        .then((res: any) => {
          this.loadingProvider.hide();
          let credential = auth.FacebookAuthProvider.credential(
            res.credential.accessToken
          );
          this.afAuth.auth
            .signInWithCredential(credential)
            .then(() => {
              if (res.additionalUserInfo.isNewUser) {
                let uid = this.afAuth.auth.currentUser.uid;
                let userInfo = res.additionalUserInfo.profile;
                this.createNewUser(
                  uid,
                  userInfo.name,
                  uid,
                  userInfo.phoneNumber,
                  userInfo.email,
                  "Available",
                  "Facebook",
                  userInfo.picture
                );
              } else {
                localStorage.setItem("isLoggedIn", "true");
                this.router.navigateByUrl("/");
              }
            })
            .catch((err) => console.log(err));
        })
        .catch((err) => {
          console.log(err);
          this.loadingProvider.hide();
        });
    } else {
      this.facebook
        .login(["public_profile", "email"])
        .then((res) => {
          console.log(res);
          let credential = auth.FacebookAuthProvider.credential(
            res.authResponse.accessToken
          );
          this.loadingProvider.show();
          this.afAuth.auth
            .signInWithCredential(credential)
            .then((res) => {
              if (res.additionalUserInfo.isNewUser) {
                this.facebook
                  .api("me/?fields=id,email,first_name,picture,gender", [
                    "public_profile",
                    "email",
                  ])
                  .then((data) => {
                    console.log(data);
                    let uid = this.afAuth.auth.currentUser.uid;
                    this.createNewUser(
                      uid,
                      data.first_name,
                      uid,
                      data.phoneNumber,
                      data.email,
                      "I am available",
                      "Facebook",
                      data.picture.data.url
                    );
                  })
                  .catch((err) => {
                    console.log(err);
                    this.loadingProvider.hide();
                  });
              } else {
                localStorage.setItem("isLoggedIn", "true");
                this.router.navigateByUrl("/");
              }
            })
            .catch((error) => {
              this.loadingProvider.hide();
            });
        })
        .catch((err) => console.log(err));
    }
  }

  gLogin() {
    if (this.platform.is("desktop")) {
      this.afAuth.auth
        .signInWithPopup(new auth.GoogleAuthProvider())
        .then((res: any) => {
          let credential = auth.GoogleAuthProvider.credential(
            res.credential.idToken,
            res.credential.accessToken
          );
          this.afAuth.auth
            .signInWithCredential(credential)
            .then(() => {
              if (res.additionalUserInfo.isNewUser) {
                let uid = this.afAuth.auth.currentUser.uid;
                let userInfo = res.additionalUserInfo.profile;
                this.createNewUser(
                  uid,
                  userInfo.name,
                  uid,
                  userInfo.phoneNumber,
                  userInfo.email,
                  "Available",
                  "Google",
                  userInfo.picture
                );
              } else {
                localStorage.setItem("isLoggedIn", "true");
                this.router.navigateByUrl("/");
              }
            })
            .catch((err) => {
              console.log("Err! signInWithCredential" + err);
            });
        })
        .catch((err) => {
          console.log("Err! signInWithCredential" + err);
        });
    } else {
      this.gplus
        .login({
          webClientId: environment.googleClientId,
        })
        .then((result: any) => {
          let credential = auth.GoogleAuthProvider.credential(
            result["token"],
            null
          );
          this.afAuth.auth
            .signInWithCredential(credential)
            .then((res: any) => {
              if (res.additionalUserInfo.isNewUser) {
                let uid = this.afAuth.auth.currentUser.uid;
                let userInfo = res.additionalUserInfo.profile;
                this.createNewUser(
                  uid,
                  userInfo.name,
                  uid,
                  userInfo.phoneNumber,
                  userInfo.email,
                  "Available",
                  "Google",
                  userInfo.picture
                );
              } else {
                localStorage.setItem("isLoggedIn", "true");
                this.router.navigateByUrl("/");
              }
            })
            .catch((err) => {
              console.log("Err! signInWithCredential" + err);
            });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  createNewUser(
    userId,
    name,
    username,
    phoneNumber,
    email,
    description = "I'm available",
    provider,
    img = "./assets/images/default-dp.png"
  ) {
    let dateCreated = new Date();
    this.afdb
      .collection("/accounts/")
      .doc(userId)
      .set({
        dateCreated,
        username,
        name,
        userId,
        phoneNumber,
        email,
        description,
        provider,
        img,
      })
      .then(() => {
        localStorage.setItem("isLoggedIn", "true");
        this.router.navigateByUrl("/");
      });
  }

  getUserData(uid) {
    return this.afdb.collection("/accounts/").doc(uid).valueChanges();
  }

  setUser(user) {
    this.user = user;
    return this.user;
  }

  getUser() {
    return this.user;
  }

  updateUser(obj) {
    return this.afdb
      .collection("/accounts/")
      .doc(this.afAuth.auth.currentUser.uid)
      .update(obj);
  }

  logout() {
    this.afAuth.auth.signOut().then(() => {
      localStorage.clear();
      this.router.navigateByUrl("/on-boarding", {
        replaceUrl: true,
        skipLocationChange: true,
      });
    });
  }
}
