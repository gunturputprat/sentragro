import { Injectable } from "@angular/core";
import {
  LoadingController,
  ToastController,
  ActionSheetController,
  AlertController,
} from "@ionic/angular";

@Injectable({
  providedIn: "root",
})
export class LoadingService {
  private loading;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private actionCtrl: ActionSheetController,
    private alertCtrl: AlertController
  ) {}

  show() {
    if (!this.loading) {
      this.loadingCtrl
        .create({ spinner: "circles", duration: 2000 })
        .then((res) => {
          this.loading = res;
          this.loading.present();
        });
    }
  }
  hide() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  showToast(message) {
    this.toastCtrl
      .create({ position: "top", message: message, duration: 3000 })
      .then((r) => r.present());
  }

  async alertConfirm() {
    const alert = await this.alertCtrl.create({
      cssClass: "my-custom-class",
      header: "Yakin ingin menghapus produk ini?",
      message: "Message <strong>text</strong>!!!",
      buttons: [
        {
          text: "Tidak",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Confirm Cancel: blah");
          },
        },
        {
          text: "Iya",
          handler: () => {
            console.log("Confirm Okay");
          },
        },
      ],
    });

    await alert.present();
  }

  async showAction() {
    const actionSheet = await this.actionCtrl.create({
      header: "Albums",
      cssClass: "my-custom-class",
      buttons: [
        {
          text: "Hapus",
          role: "destructive",
          // icon: "trash",
          handler: () => {
            console.log("Delete clicked");
          },
        },
        {
          text: "Ubah produk",
          // icon: "share",
          handler: () => {
            console.log("Share clicked");
          },
        },
        {
          text: "Cancel",
          // icon: "close",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
      ],
    });
    await actionSheet.present();
  }
}
