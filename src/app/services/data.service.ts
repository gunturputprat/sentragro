import { Injectable } from "@angular/core";
import { AngularFireDatabase, AngularFireAction } from "@angular/fire/database";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
@Injectable({
  providedIn: "root",
})
export class DataService {
  auth;
  constructor(private afAuth: AngularFireAuth, private afdb: AngularFirestore) {
    this.auth = this.afAuth.auth;
  }

  // Get all users
  getUsers() {
    return this.afdb.collection("/accounts", (ref) => ref.orderBy("name"));
  }

  searchUser(keyword) {
    return this.afdb.collection("/accounts", (ref) =>
      ref
        .orderBy("name")
        .startAt(keyword)
        .endAt(keyword + "\uf8ff")
    );
  }

  // Get user with username
  getUserWithUsername(username) {
    return this.afdb.collection("/accounts", (ref) =>
      ref.orderBy("username", username)
    );
  }

  // Get logged in user data
  getCurrentUser() {
    return this.afdb.collection("/accounts/").doc(this.auth.currentUser.uid);
  }

  // Get user by their userId
  getUser(userId) {
    return this.afdb.collection("/accounts/").doc(userId);
  }

  // Get requests given the userId.
  getRequests(userId) {
    return this.afdb.collection("/requests/").doc(userId);
  }

  // Get friend requests given the userId.
  getFriendRequests(userId) {
    return this.afdb.collection("/requests", (ref) =>
      ref.orderBy("receiver", userId)
    );
  }

  // Get conversation given the conversationId.
  getConversation(conversationId) {
    return this.afdb.collection("/conversations/").doc(conversationId);
  }

  // Get conversations of the current logged in user.
  getConversations() {
    return this.afdb.collection(
      "/accounts/" + this.auth.currentUser.uid + "/conversations"
    );
  }

  // Get messages of the conversation given the Id.
  getConversationMessages(conversationId) {
    return this.afdb.collection("/conversations/").doc(conversationId);
  }
  // Get messages of the group given the Id.
  getGroupMessages(groupId) {
    return this.afdb.collection("/groups/" + groupId + "/messages");
  }

  // Get groups of the logged in user.
  getGroups() {
    return this.afdb.collection(
      "/accounts/" + this.auth.currentUser.uid + "/groups"
    );
  }

  // Get group info given the groupId.
  getGroup(groupId) {
    return this.afdb.collection("/groups/").doc(groupId);
  }

  getBlockedLists() {
    // return this.afdb.collection('/accounts/' + this.afAuth.auth.currentUser.uid + '/conversations', ref => ref.orderBy('blocked').equalTo(true));
    return this.afdb
      .collection("/accounts/", (ref) => ref.orderBy("blocked"))
      .doc(this.auth.currentUser.uid + "/conversations");
  }

  getUserByPhone() {
    return this.afdb.collection("accounts");
  }

  //
  setProducts(time) {
    return this.afdb.collection("products").doc(time);
  }

  getProducts(userId) {
    return this.afdb.collection("/accounts/" + userId + "/products");
  }

  getProduct(productId) {
    return this.afdb.collection("/products").doc(productId);
  }

  deleteProduct(userId, productId) {
    return this.afdb
      .collection("/accounts/" + userId + "/products")
      .doc(productId);
  }
  //
  setFarmers(time) {
    return this.afdb.collection("farmers").doc(time);
  }

  getFarmers(userId) {
    return this.afdb.collection("/accounts/" + userId + "/farmers");
  }

  getFarmer(farmersId) {
    return this.afdb.collection("/farmers").doc(farmersId);
  }

  deleteFarmer(userId, farmersId) {
    return this.afdb
      .collection("/accounts/" + userId + "/farmers")
      .doc(farmersId);
  }
}
