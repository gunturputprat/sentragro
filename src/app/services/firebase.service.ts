import { Injectable } from '@angular/core';
import { LoadingService } from './loading.service';
import { DataService } from './data.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(
    public angularfire: AngularFirestore,
    public loadingProvider: LoadingService,
    private afAuth: AngularFireAuth,
    private dataProvider: DataService) {
    console.log("Initializing Firebase Provider");
  }

  // Send friend request to userId.
  sendFriendRequest(userId) {
    let loggedInUserId = this.afAuth.auth.currentUser.uid;
    this.loadingProvider.show();

    var requestsSent;
    // Use take(1) so that subscription will only trigger once.
    this.dataProvider.getRequests(loggedInUserId).snapshotChanges().pipe(take(1)).subscribe((requests: any) => {
      console.log(requests.payload.id);
      if (requests.payload.id != null && requests.payload.id.requestsSent != null)
        requestsSent = requests.payload.id.requestsSent;

      if (requestsSent == null || requestsSent == undefined) {
        requestsSent = [userId];
      } else {
        if (requestsSent.indexOf(userId) == -1)
          requestsSent.push(userId);
      }
      // Add requestsSent information.
      console.log('loggedInUserId',loggedInUserId)
      this.angularfire.collection('/requests/').doc(loggedInUserId).set({
        requestsSent: requestsSent
      }).then((success) => {
        var friendRequests;
        this.dataProvider.getRequests(userId).snapshotChanges().pipe(take(1)).subscribe((requests: any) => {
          if (requests.payload.id != null && requests.payload.id.friendRequests != null)
            friendRequests = requests.payload.id.friendRequests;

          if (friendRequests == null) {
            friendRequests = [loggedInUserId];
          } else {
            if (friendRequests.indexOf(userId) == -1)
              friendRequests.push(loggedInUserId);
          }
          // Add friendRequest information.
          this.angularfire.collection('/requests/').doc(userId).set({
            friendRequests: friendRequests
          }).then((success) => {
            this.loadingProvider.hide();
            this.loadingProvider.showToast("Friend Request Sent")
            // this.alertProvider.showFriendRequestSent();
          }).catch((error) => {
            this.loadingProvider.hide();
          });
        });
      }).catch((error) => {
        this.loadingProvider.hide();
      });
    });
  }

  // Cancel friend request sent to userId.
  cancelFriendRequest(userId) {
    let loggedInUserId = this.afAuth.auth.currentUser.uid;
    this.loadingProvider.show();

    var requestsSent;
    this.dataProvider.getRequests(loggedInUserId).snapshotChanges().pipe(take(1)).subscribe((requests: any) => {
      requestsSent = requests.payload.id.requestsSent;
      requestsSent.splice(requestsSent.indexOf(userId), 1);
      // Update requestSent information.
      this.angularfire.collection('/requests/').doc(loggedInUserId).set({
        requestsSent: requestsSent
      }).then((success) => {
        var friendRequests;
        this.dataProvider.getRequests(userId).snapshotChanges().pipe(take(1)).subscribe((requests: any) => {
          friendRequests = requests.payload.id.friendRequests;
          console.log(friendRequests);
          friendRequests.splice(friendRequests.indexOf(loggedInUserId), 1);
          // Update friendRequests information.
          this.angularfire.collection('/requests/').doc(userId).set({
            friendRequests: friendRequests
          }).then((success) => {
            this.loadingProvider.hide();
            this.loadingProvider.showToast("Removed Friend Request");
            // this.alertProvider.showFriendRequestRemoved();
          }).catch((error) => {
            this.loadingProvider.hide();
          });
        });
      }).catch((error) => {
        this.loadingProvider.hide();
      });
    });
  }

  // Delete friend request.
  deleteFriendRequest(userId) {
    let loggedInUserId = this.afAuth.auth.currentUser.uid;
    this.loadingProvider.show();

    var friendRequests;
    this.dataProvider.getRequests(loggedInUserId).snapshotChanges().pipe(take(1)).subscribe((requests: any) => {
      console.log("requests deleteFriendRequest", requests)
      friendRequests = requests.payload.id.friendRequests;
      console.log(friendRequests);
      friendRequests.splice(friendRequests.indexOf(userId), 1);
      // Update friendRequests information.
      this.angularfire.collection('/requests/').doc(loggedInUserId).set({
        friendRequests: friendRequests
      }).then((success) => {
        var requestsSent;
        this.dataProvider.getRequests(userId).snapshotChanges().pipe(take(1)).subscribe((requests: any) => {
          requestsSent = requests.payload.id.requestsSent;
          requestsSent.splice(requestsSent.indexOf(loggedInUserId), 1);
          // Update requestsSent information.
          this.angularfire.collection('/requests/').doc(loggedInUserId).set({
            requestsSent: requestsSent
          }).then((success) => {
            this.loadingProvider.hide();

          }).catch((error) => {
            this.loadingProvider.hide();
          });
        });
      }).catch((error) => {
        this.loadingProvider.hide();
        //TODO ERROR
      });
    });
  }

  // Accept friend request.
  acceptFriendRequest(userId) {
    let loggedInUserId = this.afAuth.auth.currentUser.uid;
    // Delete friend request.
    this.deleteFriendRequest(userId);

    this.loadingProvider.show();
    this.dataProvider.getUser(loggedInUserId).snapshotChanges().pipe(take(1)).subscribe((account: any) => {
      var friends = account.payload.id.friends;
      if (!friends) {
        friends = [userId];
      } else {
        friends.push(userId);
      }
      // Add both users as friends.
      this.dataProvider.getUser(loggedInUserId).set({
        friends: friends
      }).then((success) => {
        this.dataProvider.getUser(userId).snapshotChanges().pipe(take(1)).subscribe((account: any) => {
          var friends = account.payload.id.friends;
          if (!friends) {
            friends = [loggedInUserId];
          } else {
            friends.push(loggedInUserId);
          }
          this.dataProvider.getUser(userId).set({
            friends: friends
          }).then((success) => {
            this.loadingProvider.hide();
          }).catch((error) => {
            this.loadingProvider.hide();
          });
        });
      }).catch((error) => {
        this.loadingProvider.hide();
      });
    });
  }
}
