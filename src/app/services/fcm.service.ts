import { Injectable } from "@angular/core";
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { Platform } from "@ionic/angular";
import { AngularFirestore } from "@angular/fire/firestore";
import { HTTP } from "@ionic-native/http/ngx";

@Injectable({
  providedIn: "root",
})
export class FcmService {
  cloudMessegingId =
    "key=AAAAcC3WoP0:APA91bGt1Iv8bmH0RcwJfE-F-dSv6cGTonqOMH4by619dH-WSCH-BIEh_qTHX7bvaK8ByF5RpZ3BtQaKS_nqux3CnFBpggRmq4MYd15u-I0RX_KWUyaMliWQ6rOpavz_87p9904zbsmp";
  constructor(
    private firebase: FirebaseX,
    private afs: AngularFirestore,
    private platform: Platform,
    private http: HTTP
  ) {}
  async bypassNotification(name, message, token) {
    console.log("bypassNotification", name, message, token);
    // json to bypas
    let payload = {
      notification: {
        title: name,
        body: message,
        sound: "default",
        click_action: "FCM_PLUGIN_ACTIVITY",
        icon: "fcm_push_icon",
      },
      to: token,
      priority: "high",
      restricted_package_name: "",
    };
    let headers = {
      "Content-Type": "application/json",
      Authorization: this.cloudMessegingId,
    };
    console.log("si payload", payload);
    console.log("si headers", headers);

    this.http
      .post("https://fcm.googleapis.com/fcm/send", payload, headers)
      .then((data) => {
        console.log("feedback data notif", data);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  async getToken() {
    let token;

    if (this.platform.is("android")) {
      token = await this.firebase.getToken();
    }

    if (this.platform.is("ios")) {
      token = await this.firebase.getToken();
      await this.firebase.grantPermission();
    }

    this.saveToken(token);
  }

  private saveToken(token) {
    if (!token) return;

    const devicesRef = this.afs.collection("devices");

    const data = {
      token,
      userId: "testUserId",
    };

    return devicesRef.doc(token).set(data);
  }

  onNotifications() {
    return this.firebase.onMessageReceived();
  }
}
