import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { DataService } from "../services/data.service";
import { LoadingService } from "../services/loading.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Validator } from "src/environments/validator";
import { AlertController } from "@ionic/angular";
import { ImageService } from "../services/image.service";
import { Camera } from "@ionic-native/camera/ngx";

@Component({
  selector: "app-farmers-detail",
  templateUrl: "./farmers-detail.page.html",
  styleUrls: ["./farmers-detail.page.scss"],
})
export class FarmersDetailPage implements OnInit {
  farmerId: any;
  farmer: any;

  myForm: FormGroup;
  submitAttempt = false;
  errorMessages: any = [];
  currentUser: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dataProvider: DataService,
    private loadingProvider: LoadingService,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private imageProvider: ImageService,
    private camera: Camera
  ) {
    this.errorMessages = Validator.errorMessages;
    this.myForm = this.formBuilder.group({
      farmer_name: Validator.farmer_nameValidator,
      phoneNumber: Validator.phoneNumberValidator,
      plantation: Validator.plantationValidator,
      address: Validator.addressValidator,
    });
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.loadingProvider.show();
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    this.farmerId = this.route.snapshot.params.id;
    this.getFarmer(this.farmerId);
  }

  getFarmer(farmerId) {
    console.log("farmerId", farmerId);
    this.dataProvider
      .getFarmer(farmerId)
      .valueChanges()
      .subscribe((farmer) => {
        this.farmer = farmer;
        console.log("farmer", this.farmer);
        this.loadingProvider.hide();
      });
  }

  // option() {
  //   this.loadingProvider.showAction();
  // }

  setPhoto() {
    this.alertCtrl
      .create({
        header: "Set Profile Photo",
        message:
          "Do you want to take a photo or choose from your photo gallery?",
        buttons: [
          {
            text: "Cancel",
            handler: (data) => {},
          },
          {
            text: "Choose from Gallery",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setFarmerPhoto(
                this.farmer.farmerId,
                this.camera.PictureSourceType.PHOTOLIBRARY
              );
            },
          },
          {
            text: "Take Photo",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setFarmerPhoto(
                this.farmer.farmerId,
                this.camera.PictureSourceType.CAMERA
              );
            },
          },
        ],
      })
      .then((r) => r.present());
  }

  setPhotoKTP() {
    this.alertCtrl
      .create({
        header: "Set KTP Photo",
        message:
          "Do you want to take a photo or choose from your photo gallery?",
        buttons: [
          {
            text: "Cancel",
            handler: (data) => {},
          },
          {
            text: "Choose from Gallery",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setKTPFarmerPhoto(
                this.farmer.farmerId,
                this.camera.PictureSourceType.PHOTOLIBRARY
              );
            },
          },
          {
            text: "Take Photo",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setKTPFarmerPhoto(
                this.farmer.farmerId,
                this.camera.PictureSourceType.CAMERA
              );
            },
          },
        ],
      })
      .then((r) => r.present());
  }

  save() {
    this.submitAttempt = true;
    if (this.myForm.valid) {
      this.loadingProvider.show();
      // add new farmer
      this.dataProvider
        .getFarmer(this.farmer.farmerId)
        .update(this.farmer)
        .then((response) => {
          console.log("response upload farmer", response);
          this.loadingProvider.showToast("Berhasil mengubah produk");
          this.loadingProvider.hide();
        })
        .catch((err) => {
          this.loadingProvider.showToast(
            "Terjadi kesalahan, silahkan coba lagi"
          );
          this.loadingProvider.hide();
        });
    }
  }
  async alertDelete() {
    const alert = await this.alertCtrl.create({
      cssClass: "my-custom-class",
      header: "Yakin ingin menghapusnya?",
      message: "Petani akan di hapus secara permanen.",
      buttons: [
        {
          text: "Tidak",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Confirm Cancel: blah");
          },
        },
        {
          text: "Iya",
          handler: () => {
            this.delete();
          },
        },
      ],
    });

    await alert.present();
  }
  delete() {
    this.loadingProvider.show();
    this.dataProvider
      .deleteFarmer(this.farmer.userId, this.farmer.farmerId)
      .delete()
      .then((res) => {
        this.dataProvider
          .getFarmer(this.farmer.farmerId)
          .delete()
          .then((farmer) => {
            this.imageProvider.deleteFarmerImage(
              this.farmer.farmerId,
              this.farmer.img,
              this.farmer.ktp
            );
            console.log("deleted", farmer);
            this.router.navigateByUrl("/tabs/tab2");
            this.loadingProvider.hide();
          })
          .catch((err) => {
            alert(
              "Ada kesalahan dalam menghapus produk, silahkan hubungi tim kami."
            );
          });
      })
      .catch((err) => {
        alert(
          "Ada kesalahan dalam menghapus produk, silahkan hubungi tim kami."
        );
      });
  }
}
