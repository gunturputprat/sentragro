import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    loadChildren: () =>
      import("./tabs/tabs.module").then((m) => m.TabsPageModule),
  },
  {
    path: "friends",
    loadChildren: "./friends/friends.module#FriendsPageModule",
  },
  { path: "group/:id", loadChildren: "./group/group.module#GroupPageModule" },
  {
    path: "addmembers/:id",
    loadChildren: "./addmembers/addmembers.module#AddmembersPageModule",
  },
  {
    path: "groupinfo/:id",
    loadChildren: "./groupinfo/groupinfo.module#GroupinfoPageModule",
  },
  { path: "groups", loadChildren: "./groups/groups.module#GroupsPageModule" },
  { path: "login", loadChildren: "./login/login.module#LoginPageModule" },
  {
    path: "imagemodal",
    loadChildren: "./imagemodal/imagemodal.module#ImagemodalPageModule",
  },
  {
    path: "message/:id",
    loadChildren: "./message/message.module#MessagePageModule",
  },
  {
    path: "messages",
    loadChildren: "./messages/messages.module#MessagesPageModule",
  },
  {
    path: "newgroup",
    loadChildren: "./newgroup/newgroup.module#NewgroupPageModule",
  },
  {
    path: "blockedlist",
    loadChildren: "./blockedlist/blockedlist.module#BlockedlistPageModule",
  },
  {
    path: "register",
    loadChildren: "./register/register.module#RegisterPageModule",
  },
  {
    path: "userinfo/:id",
    loadChildren: "./userinfo/userinfo.module#UserinfoPageModule",
  },
  {
    path: "profile",
    loadChildren: "./profile/profile.module#ProfilePageModule",
  },
  { path: "forgot", loadChildren: "./forgot/forgot.module#ForgotPageModule" },
  {
    path: "settings",
    loadChildren: "./settings/settings.module#SettingsPageModule",
  },
  {
    path: "contact",
    loadChildren: "./contact/contact.module#ContactPageModule",
  },
  {
    path: "on-boarding",
    loadChildren: "./on-boarding/on-boarding.module#OnBoardingPageModule",
  },
  {
    path: "stock-add",
    loadChildren: "./stock-add/stock-add.module#StockAddPageModule",
  },
  { path: "stock", loadChildren: "./stock/stock.module#StockPageModule" },
  {
    path: "cashflow",
    loadChildren: "./cashflow/cashflow.module#CashflowPageModule",
  },
  {
    path: "farmers",
    loadChildren: "./farmers/farmers.module#FarmersPageModule",
  },
  {
    path: "farmers-add",
    loadChildren: "./farmers-add/farmers-add.module#FarmersAddPageModule",
  },
  {
    path: "stock-detail/:id",
    loadChildren: "./stock-detail/stock-detail.module#StockDetailPageModule",
  },
  {
    path: "farmers-detail/:id",
    loadChildren:
      "./farmers-detail/farmers-detail.module#FarmersDetailPageModule",
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
