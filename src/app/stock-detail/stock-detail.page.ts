import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { DataService } from "../services/data.service";
import { LoadingService } from "../services/loading.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Validator } from "src/environments/validator";
import { AlertController } from "@ionic/angular";
import { ImageService } from "../services/image.service";
import { Camera } from "@ionic-native/camera/ngx";

@Component({
  selector: "app-stock-detail",
  templateUrl: "./stock-detail.page.html",
  styleUrls: ["./stock-detail.page.scss"],
})
export class StockDetailPage implements OnInit {
  productId: any;
  product: any;

  myForm: FormGroup;
  submitAttempt = false;
  errorMessages: any = [];
  currentUser: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dataProvider: DataService,
    private loadingProvider: LoadingService,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private imageProvider: ImageService,
    private camera: Camera
  ) {
    this.errorMessages = Validator.errorMessages;
    this.myForm = this.formBuilder.group({
      product_name: Validator.product_nameValidator,
      category: Validator.categoryValidator,
      price: Validator.priceValidator,
      weight: Validator.weightValidator,
      benefits: Validator.benefitsValidator,
      ingredients: Validator.ingredientsValidator,
    });
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.loadingProvider.show();
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    this.productId = this.route.snapshot.params.id;
    this.getProduct(this.productId);
  }

  getProduct(productId) {
    console.log("productid", productId);
    this.dataProvider
      .getProduct(productId)
      .valueChanges()
      .subscribe((product) => {
        this.product = product;
        console.log("product", this.product);
        this.loadingProvider.hide();
      });
  }

  // option() {
  //   this.loadingProvider.showAction();
  // }

  setPhoto() {
    this.alertCtrl
      .create({
        header: "Set Profile Photo",
        message:
          "Do you want to take a photo or choose from your photo gallery?",
        buttons: [
          {
            text: "Cancel",
            handler: (data) => {},
          },
          {
            text: "Choose from Gallery",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setProductPhoto(
                this.product.productId,
                this.camera.PictureSourceType.PHOTOLIBRARY
              );
            },
          },
          {
            text: "Take Photo",
            handler: () => {
              // Call imageProvider to process, upload, and update user photo.
              this.imageProvider.setProductPhoto(
                this.product.productId,
                this.camera.PictureSourceType.CAMERA
              );
            },
          },
        ],
      })
      .then((r) => r.present());
  }

  save() {
    this.submitAttempt = true;
    if (this.myForm.valid) {
      this.loadingProvider.show();
      // add new product
      this.dataProvider
        .getProduct(this.product.productId)
        .update(this.product)
        .then((response) => {
          console.log("response upload product", response);
          this.loadingProvider.showToast("Berhasil mengubah produk");
          this.loadingProvider.hide();
        })
        .catch((err) => {
          this.loadingProvider.showToast(
            "Terjadi kesalahan, silahkan coba lagi"
          );
          this.loadingProvider.hide();
        });
    }
  }
  async alertDelete() {
    const alert = await this.alertCtrl.create({
      cssClass: "my-custom-class",
      header: "Yakin ingin menghapusnya?",
      message: "Produk akan di hapus secara permanen.",
      buttons: [
        {
          text: "Tidak",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Confirm Cancel: blah");
          },
        },
        {
          text: "Iya",
          handler: () => {
            this.delete();
          },
        },
      ],
    });

    await alert.present();
  }
  delete() {
    this.loadingProvider.show();
    this.dataProvider
      .deleteProduct(this.product.userId, this.product.productId)
      .delete()
      .then((res) => {
        this.dataProvider
          .getProduct(this.product.productId)
          .delete()
          .then((product) => {
            this.imageProvider.deleteProductImage(
              this.product.productId,
              this.product.img
            );
            console.log("deleted", product);
            this.router.navigateByUrl("/");
            this.loadingProvider.hide();
          })
          .catch((err) => {
            alert(
              "Ada kesalahan dalam menghapus produk, silahkan hubungi tim kami."
            );
          });
        this.imageProvider.deleteProductImage(
          this.product.productId,
          this.product.img
        );
      })
      .catch((err) => {
        alert(
          "Ada kesalahan dalam menghapus produk, silahkan hubungi tim kami."
        );
      });
  }
}
