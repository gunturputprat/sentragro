import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireAuthModule } from "@angular/fire/auth";

import { environment } from "../environments/environment.prod";

import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { Facebook } from "@ionic-native/facebook/ngx";
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { Camera } from "@ionic-native/camera/ngx";
import { MediaCapture } from "@ionic-native/media-capture/ngx";
import {
  FileTransferObject,
  FileTransfer,
} from "@ionic-native/file-transfer/ngx";
import { File } from "@ionic-native/file/ngx";
import { FileChooser } from "@ionic-native/file-chooser/ngx";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { Contacts } from "@ionic-native/contacts/ngx";
import { Keyboard } from "@ionic-native/keyboard/ngx";
import { FCM } from "@ionic-native/fcm/ngx";
import { SharedModule } from "./services/share.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { ImagemodalPageModule } from "./imagemodal/imagemodal.module";
import {
  AngularFirestore,
  AngularFirestoreModule,
} from "@angular/fire/firestore";
import { FirebaseAuthentication } from "@ionic-native/firebase-authentication/ngx";
import { HTTP } from "@ionic-native/http/ngx";
import { FilePath } from "@ionic-native/file-path/ngx";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
      mode: "ios",
    }),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ImagemodalPageModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GooglePlus,
    Facebook,
    Camera,
    MediaCapture,
    File,
    FirebaseX,
    FirebaseAuthentication,
    Geolocation,
    Contacts,
    Keyboard,
    FileTransfer,
    FileChooser,
    FileTransferObject,
    FCM,
    HTTP,
    FilePath,
    InAppBrowser,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
