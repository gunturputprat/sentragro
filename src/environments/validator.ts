import { Validators } from "@angular/forms";

export namespace Validator {
  // user
  export const emailValidator = [
    "",
    [
      Validators.required,
      Validators.pattern(
        "^[a-z0-9]+(.[_a-z0-9]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,15})$"
      ),
    ],
  ];

  export const passwordValidator = [
    "",
    [Validators.required, Validators.minLength(6)],
  ];

  export const nameValidator = ["", [Validators.required]];

  export const usernameValidator = [
    "",
    [Validators.required, Validators.minLength(5)],
  ];

  export const bioValidator = ["", [Validators.required]];

  export const groupNameValidator = ["", [Validators.required]];

  export const groupDescriptionValidator = ["", [Validators.required]];

  // product
  export const product_nameValidator = ["", [Validators.required]];
  export const categoryValidator = ["", [Validators.required]];
  export const priceValidator = ["", [Validators.required]];
  export const weightValidator = ["", [Validators.required]];
  export const benefitsValidator = ["", [Validators.required]];
  export const ingredientsValidator = ["", [Validators.required]];

  // farmers
  export const farmer_nameValidator = ["", [Validators.required]];
  export const phoneNumberValidator = ["", [Validators.required]];
  export const plantationValidator = ["", [Validators.required]];
  export const addressValidator = ["", [Validators.required]];

  export const errorMessages = {
    //   users
    email: [
      { type: "required", message: "Email wajib diisi" },
      { type: "pattern", message: "Email salah, silahkan coba lagi" },
    ],
    password: [
      { type: "required", message: "Password wajib diisi" },
      { type: "minlength", message: "Password minimal 6 karakter" },
    ],
    name: [{ type: "required", message: "Nama wajib diisi" }],
    username: [
      { type: "required", message: "Username wajib diisi" },
      { type: "minlength", message: "Username minimal 5 karakter" },
    ],
    bio: [{ type: "required", message: "Bio wajib diisi" }],
    groupName: [{ type: "required", message: "Nama grup wajib diisi" }],
    groupDescription: [
      { type: "required", message: "Deskripsi grup wajib diisi" },
    ],

    // product
    product_name: [{ type: "required", message: "Nama produk wajib diisi" }],
    category: [{ type: "required", message: "Kategori produk wajib diisi" }],
    price: [{ type: "required", message: "Harga produk wajib diisi" }],
    weight: [{ type: "required", message: "Kemasan produk wajib diisi" }],
    benefits: [{ type: "required", message: "Manfaat produk wajib diisi" }],
    ingredients: [
      { type: "required", message: "Kandungan produk wajib diisi" },
    ],
  };
}
