export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBQkFm3XgxgYqDSbEvn5IFYcaKG5RKZqvY",
    authDomain: "sentragro-app.firebaseapp.com",
    databaseURL: "https://sentragro-app.firebaseio.com",
    projectId: "sentragro-app",
    storageBucket: "sentragro-app.appspot.com",
    messagingSenderId: "160253751929",
    appId: "1:160253751929:web:15004b89318fe96ffb9aa8",
    measurementId: "G-3SXSNQTHMJ",
  },
  googleClientId:
    "845839389008-s0scp3mghdi67t5ga9t56j6265ibonp5.apps.googleusercontent.com",
};
